def dotify(hash, k = [])
  return {k.join('.') => hash} unless hash.is_a?(Hash)
  hash.inject({}){ |h, v| h.merge! dotify(v[-1], k + [v[0]]) }
end

describe "dotify" do
  let(:hash) { { foo: { bar: { baz: true } } } }

  it "converts hash to dot notated keys" do
    expect(dotify(hash)).to eq("foo.bar.baz" => true)
  end
end